Component({
  data: {
    headersLen: '',//表头数量
  },
  props: {
    tableData: [],
    columns: [],
    
    tbodyMaxHeight: ''//表格body最大高度
  },
  didMount() {
    let columns = this.props.columns
    let num = 0
    for (let i = 0; i < columns.length; i++) {
      num = num + parseInt(columns[i].width)
    }
    this.setData({headersLen: num})    
  },
  didUpdate() { },
  didUnmount() { },
  methods: {}
});