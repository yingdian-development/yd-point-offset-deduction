import { createForm } from 'antd-mini/es/Form/form';

Component({
  mixins: [createForm()],
  data: {
    visible: false,
    oldObj: {},
    newObj: {}
  },
  methods: {
    showPopup() {
      this.setData({
        visible: true,
      });
    },
    handlePopupClose() {
      this.setData({
        visible: false,
      });
    },
    onChange(value, e) {
      console.log(e)

      this.emit('onChange', e);
      this.setData({oldObj: e})

      this.props.onPriceValue(e)
    },
    onClose(){

    },
    onInput(value, e){
      this.props.onInputChangeValue(value)
    },
    submit(){
      let oldObj = this.data.oldObj

      if(oldObj.title == '' || oldObj.title == undefined){
        my.showToast({ content: '请选择姓名', duration: 1000 });
        return
      }

      this.setData({
        newObj: oldObj,
        visible: false
      })

      this.handlePopupClose()
    }
  }
});