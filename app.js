import 'dingtalk-jsapi/entry/mobile';
import roleTabBar from './role-tabbar.json';

App({
  corpId: 'dingb15c8ca804be3b8effe93478753d9884',
  agentId: '3226479918',
  // api: 'http://frps.ydkj.vip:7000',
  // api: 'http://127.0.0.1:81',
  // api:'http://192.168.1.156:81',
  haslogin: false,
  api: 'https://recruit.ydkj.vip',
  userData: dd.getStorageSync({ key: 'userdata' }),
  onShow(){
    this.updateManager()
  },
  onLaunch(options) {
    let that = this
    that.login()
  },
  login: async function () {
    let that = this
    return new Promise((resolve, reject) => {
      if (that.haslogin) {
        console.log('登录过了')
        resolve( dd.getStorageSync({ key: 'token' }).data)
      } else {
        dd.getAuthCode({
          success: function (res) {
            dd.httpRequest({
              headers: {
                "Content-Type": "application/json"
              },  
              url: that.api + '/api/login',
              data: JSON.stringify({ authCode: res.authCode }),
              method: 'POST',
              dataType: 'json',
              success: (ret) => {
                if (ret.data.errcode != 0) {
                  my.showToast({
                    type: 'exception',
                    content: '网络异常',
                    duration: 2000
                  });
                  reject(res)
                } else {
                  dd.setStorageSync({
                    key: 'token',
                    data: ret.data.data.userid
                  })

                  dd.setStorageSync({
                    key: 'userdata',
                    data: ret.data.data
                  });
                  console.log('登录成功')
                  that.userData = dd.getStorageSync({ key: 'userdata' })
                  that.haslogin = true
                  // that.setTabBar()
                  resolve(dd.getStorageSync({ key: 'token' }).data)
                }
              },
              fail: (res) => {
                my.showToast({
                  type: 'exception',
                  content: '网络异常',
                  duration: 2000
                });
                reject(res)
              }
            })
          }
        })
      }
    })
  },
  //判断登陆完成
  authLogin: function (callback) {
    var that = this;
    if (typeof callback === "function") {
      var interval = setInterval(function () {
        try{
          if (that.haslogin) {
            callback();
            clearInterval(interval);
          }
        }catch(e){
          clearInterval(interval);
          console.log(e)
        }
      }, 300);
    }
  },
  updateManager: function () {
    const updateManager = dd.getUpdateManager()

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res.hasUpdate) // 是否有更新
    })

    updateManager.onUpdateReady(function (ret) {
      console.log(ret.version) // 更新版本号
      dd.confirm({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      // 新版本下载失败
    })
  },
  //根据角色设置底部导航
  setTabBar: function () {
    let that = this
    let role_slug = that.userData.data.role_slug
    if (role_slug) {
      if (roleTabBar.tabBar.hasOwnProperty(role_slug)) {
        let aa = roleTabBar.tabBar[role_slug]
        aa.forEach(element => {
          // console.log(element)
          dd.addTabBarItem(element);
        });
      }
    }


    // let role = JSON.parse(that.userData.data.role)
    // if(role.length){
    //   for(let i=0;i<role.length;i++){
    //       // console.log(role[i],1111111111111)
    //       if(roleTabBar.tabBar.hasOwnProperty(role[i].slug)){
    //           let aa = roleTabBar.tabBar[role[i].slug]
    //           aa.forEach(element => {
    //             // console.log(element)
    //             dd.addTabBarItem(element);
    //           });
    //           that.slug = role[i].slug
    //           break;
    //       }
    //   }
    // }


  }

})