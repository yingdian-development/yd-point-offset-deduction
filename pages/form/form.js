import { Form } from 'antd-mini/es/Form/form';
import { myRequest } from '../../utils/http';
const dayjs = require('dayjs')
const app = getApp()
let account = 1;
Page({
  form: new Form({
    initialValues: {
      distribute_income: '是',
      frozen: '是'
    },
    rules:{
      name:[{required:true,message:'请输入门店名称'}],
      store_id_value:[{required:true,message:'请输入门店ID'}],
      reason:[{required:true,message:'请输入报备原因'}],
      brand:[{required:true,message:'请选择品牌'}],
      // warehouse_number:[{required:true,message:'请输入仓位数'}],
      report_date:[{required:true,message:'请选择预计处理完成日期'}],
      name_of_payee:[{required:true,message:'请输入收款人姓名'}],
      pay_bank_card:[{required:true,message:'请输入收款卡号'}],
      opening_bank:[{required:true,message:'请输入开户行'}],
      rental:[
        {required:true,message:'请输入租金'},
        (form) => ({
          async validator(_, value) {
            let reg = /^\d+(\.\d{1,2})?$/;
            if (reg.test(value)) {
              return
            }
            throw new Error('租金输入错误');
            
          },
        })
      ],
    }
  }),
  data: {
    type: '',//1新增报备
    list: [{
      sn_json:`sn_json${account++}`
    }],
    reasonOptions: [
      { value: '家庭原因', text: '家庭原因' },
      { value: '个人原因', text: '个人原因' },
      { value: '发展原因', text: '发展原因' },
      { value: '合同到期不续签', text: '合同到期不续签' },
      { value: '其他', text: '其他' }
    ],
    day:0,
    report_date:'',
    videoList0: [],
    fileList0: [],
    fileList1: [],
    fileList2: [],
    width: '',
    height: '',
    tableData: [],
    columns: [
      { title: '历史最高门店等级', field: 'highest_level', width: '150' },
      { title: '本月流水', field: 'month_gtv', width: '150' },
      { title: '上月流水', field: 'last_flow', class: '', width: '150' },
      { title: '2个月前流水', field: 'two_flow', width: '150' },
      { title: '3个月前流水', field: 'three_flow', class: '', width: '150' }
    ],
    headersLen: 750,
    tbodyMaxHeight: null,
    personOptions: []
  },
  onShow(){},
  onLoad(e){
    this.setData({
      type: e.type
      // start_time:'',
      // fileList0: [],
    })

    let that = this
    console.log(e)
    if(!dd.getStorageSync({key: 'token'}).data){
      app.login()
    }

    app.authLogin(function() {
      that.initPerson('')

      console.log(e)

      if(e.type === '1' && e.store_id_value){
        console.log(e.store_id_value)
        setTimeout(() => {
          that.form.setFieldValue('store_id_value',e.store_id_value)
          that.form.setFieldValue('name',e.name)
        }, 1000);
      }
    })

    // dd.getSystemInfo({
    //   success: function (res) {
    //     let clientHeight = res.windowHeight;// 获取可使用窗口宽度
    //     let clientWidth = res.windowWidth;// 获取可使用窗口高度
    //     let ratio = 750 / clientWidth;// 算出比例
    //     let height = clientHeight * ratio;// 算出高度(单位rpx)
        
    //     that.setData({
    //       tbodyMaxHeight: height// 设置高度
    //     });
    //   }
    // })

    // this.getNowDate()
  },
  forPrice(e){
    console.log(e)
  },
  async initPerson(name){
    let that = this;

    // if(!name) return

    let res = await myRequest('/api/get-user-subset','GET',{name})

    if(res.errcode === 0){
      let list = res.data,personOptions = []
      for(let i = 0;i < list.length;i++){
        personOptions.push({title: list[i].label, value: list[i].value })
      }

      that.setData({
        personOptions: personOptions
      }) 
    }
  },
  //获取列表
  async getList(value,e) {
    let that = this

    let res = await myRequest('/withdrawal-api/get-store-info','GET',{store_poi: value})

    that.setData({
      tableData:res.data || []
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.data.errmsg,
        duration: 2000
      });
    }
  },
  getNowDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    var day = date.getDate();
    day = day < 10 ? "0" + day : day;

    let time = year + '-' + month + '-' + day

    this.form.setInitialValues({
      start_time: time
    });
    this.form.reset();

    // return year + '-' + month + '-' + day
  },
  uploadVideo(e){
    let that = this,type = e.target.dataset.type;

    dd.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: '50',
      success: (res) => {
        const { size, width, height, duration, filePath } = res;
        // const videoPath = res.tempFiles[0].path; // 获取视频的临时路径
        console.log(res)
        that.onVideo(filePath,type); // 调用上传函数
      },
      fail: () => {},
      complete: () => {},
    });
  },
  onVideo(videoPath,type) {
    let that = this

    dd.uploadFile({
      url: app.api + '/offset-api/upload-file', // 你的服务器上传接口地址
      filePath: videoPath,
      name: 'file', // 后端接收文件的字段名
      header: {
        'X-Token': app.userData.data.userid // 其他需要一起上传的额外信息
      },
      fileType: 'video',
      success: function(res) {
        let data = JSON.parse(res.data)

        if(data.errcode == 0){// 处理上传成功
          my.showToast({ content: '上传视频成功', duration: 1000 });

          console.log('视频上传成功', res);

          let videoList = that.data['videoList' + type] || []
  
          videoList.push(data.data)
  
          that.setData({
            ['videoList' + type]: videoList
          })
        }else{
          my.showToast({ content: data.errmsg, duration: 1000 });
        }
      },
      fail: function(err) {
        console.error('视频上传失败', err);
        // 处理上传失败
        my.showToast({ content: '上传图片失败' + err, duration: 1000 });
      }
    });
  },
  uploadChange(fileList) {
    // 这里的数据包括上传失败和成功的图片列表，如果需要筛选出上传成功的图片需要在此处理
    console.log('图片列表：', fileList);
  },
  // 上传图片
  uploadImage(e) {
    let that = this,type = e.target.dataset.type;

    dd.chooseImage({ 
      sourceType: ['camera', 'album'],
      count: 1,
      success: (res1) => {
        console.log('res1', res1);
        dd.showLoading({
          content: '转码中...'
        });
        dd.compressImage({
          filePaths: res1.filePaths,
          compressLevel: 2,
          success: (res2) => {
            console.log(res2)
            // 获取图片信息
            dd.getImageInfo({
              src: res2.filePaths[0],
              success: (res) => {
                console.log(res.width, 'res')
                const { width, height, path } = res;
                that.setData({
                  width,
                  height
                });
                const ctx = dd.createCanvasContext('canvas' + type);
                ctx.drawImage(path, 0, 0, width, height);
                ctx.draw(true, () => {
                  // 获取画布指定区域的 data URL 数据
                  ctx.toDataURL({
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                    destWidth: width,
                    destHeight: height,
                  }).then(dataURL => {
                    console.log(dataURL)  // 获取base64数据
                    return dataURL
                  }).then(res => {
                    dd.hideLoading();
                    that.onUpload(res,type,width,height); // 将base64数据传给后台的方法
                  })
                })
              }
            })
          }
        });
      },
      fail: (e) => {
        console.log('fail----------------------')
      }
    })
  },
  async onUpload(file,type,width,height) {
    let res = await myRequest('/offset-api/upload','POST',{
      images: file
    })

    if(res.errcode === 0){
      let fileList = this.data['fileList' + type]
      // images= this.data['images' + type]
      // fileList.push({
      //   url: file,
      //   width: width,
      //   height: height,
      //   path: res.data
      // })

      fileList.push(res.data)
      // images.push(res.data)

      console.log(fileList)
      // console.log(images)

      let obj = {}
      obj['fileList' + type] = fileList
      // obj['images' + type] = images
      
      this.setData(obj)
    }else{
      my.showToast({ content: '上传图片失败', duration: 1000 });
    }
  },
  // 点击图片删除
  clickIcon(e) {
    let i = e.target.dataset.index,type = e.target.dataset.type;

    let fileList = this.data['fileList' + type]
    fileList.splice(i, 1);

    let obj = {}
    obj['fileList' + type] = fileList
    this.setData(obj)
  },
  // 点击视频删除
  clickIcon1(e) {
    let i = e.target.dataset.index,type = e.target.dataset.type;

    let videoList = this.data['videoList' + type]
    videoList.splice(i, 1);

    this.setData({
      ['videoList' + type]: videoList
    })
  },
  handleRef(ref) {
    this.form.addItem(ref);
  },
  reset() {
    this.form.reset();
  },
  add() {
    this.setData({
      list: [
        ... this.data.list,
        {
          sn_json: `sn_json${account++}`,
        },
      ]
    });
  },
  minus(e) {
    const { index } = e.currentTarget.dataset;
    const list = [...this.data.list];
    list.splice(index, 1);
    this.setData({
      list
    });
  },
  handleOk(date, format, e) {
    let type = e.target.dataset.type
    if(type === '1'){
      this.setData({
        report_date: format
      })
    }
  },
  async submit() {
    let that = this,type = this.data.type,obj = {},url = ''

    const values = await this.form.submit();

    if(type === '1'){
      // let resignation_user = values.resignation_user
      // if(resignation_user === undefined || resignation_user.value == '' || resignation_user === null){
      //   my.showToast({ content: '请输入实际离职用户', duration: 1000 });
      //   return
      // }

      // let last_work_date = this.data.last_work_date
      // if(last_work_date === '' || last_work_date === undefined || last_work_date === null){
      //   my.showToast({ content: '请选择最后工作日', duration: 1000 });
      //   return
      // }

      let fileList0 = this.data.fileList0 || [],videoList0 = this.data.videoList0 || [],fileList1 = this.data.fileList1 || [];
      if(fileList0.length === 0 && videoList0.length === 0 && fileList1.length === 0){
        if(fileList0.length === 0){
          my.showToast({ content: '请上传提答疑处理截图', duration: 1000 });
          return
        }

        if(videoList0.length === 0){
          my.showToast({ content: '请上传上门维护一镜到底视频', duration: 1000 });
          return
        }
  
        if(fileList1.length === 0){
          my.showToast({ content: '请上传撤机重上操作记录截图', duration: 1000 });
          return
        }
      }

      obj = {
        name: values.name,
        store_id_value: values.store_id_value,
        report_date: values.report_date,
        reason: values.reason,
        question_screenshot: fileList0,
        video: videoList0,
        operation_screenshot: fileList1
      }

      url = '/offset-api/create-store'
    }

    // values.start_time = dayjs(values.start_time).format('YYYY-MM-DD')
    // values.end_time = dayjs(values.end_time).format('YYYY-MM-DD')
    // values.day = this.data.day

    // let param = {'sn_json':[]}

    // for (const key in values) {
    //   if (key.startsWith("sn_json")) {

    //     param.sn_json.push(values[key]);
    //   }
    // }

    // let start_time = this.data.start_time,end_time = this.data.end_time;
    // if(new Date(start_time).getTime() > new Date(end_time).getTime()){
    //   my.showToast({ content: '断电开始时间不能比断电结束时间大', duration: 1000 });
    //   return
    // }

    // let images0 = this.data.fileList0
    // if(images0.length === 0){
    //   my.showToast({ content: '请上传机柜详情页截图', duration: 1000 });
    //   return
    // }

    // let detail_photo = []
    // for(let i = 0;i < images0.length; i++ ){
    //   detail_photo.push(images0[i].path)
    // }

    let res = await myRequest(url,'POST',obj)
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }

    dd.showToast({
      type: 'none',
      content: '创建成功',
      duration: 2000
    });
    that.reset()
    // dd.reLaunch({
    //   url: '/pages/index/index'
    // })

    setTimeout(() => {
      dd.navigateBack({
        delta: 1,
        success: () => {},
        fail: () => {},
        complete: () => {},
      });
    }, 1000);
  }
});