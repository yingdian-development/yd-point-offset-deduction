const app = getApp()
import { myRequest } from '../../utils/http';
Page({
  data: {
    tableData: [],
    columns: [],
    headersLen:null,
    tbodyMaxHeight: null,
    role_slug:'',
    regional_director_list: [],
    region_list: [],
    person_list: [],
    regional_director: '',//团队
    region: '',//组别
    person: ''//归属人
  },
  onShow() {
    let that = this
    dd.getSystemInfo({
      success: function (res) {
        let clientHeight = res.windowHeight;// 获取可使用窗口宽度
        let clientWidth = res.windowWidth;// 获取可使用窗口高度
        let ratio = 750 / clientWidth;// 算出比例
        let height = clientHeight * ratio;// 算出高度(单位rpx)

        let tbodyMaxHeight = height - 120,role_slug = app.userData.data.role_slug;
        if(role_slug === 'chairman' || role_slug === 'chairman_assistant' || role_slug === 'generat_manager' || role_slug === 'logistical_commissioner'){//董事长
          tbodyMaxHeight = height - 360
        }else if(role_slug === 'regional_director'){//大区总
          tbodyMaxHeight = height - 280
        }else if(role_slug === 'region'){//大区
          tbodyMaxHeight = height - 110
        }else if(role_slug === 'bd'){//大区
          tbodyMaxHeight = height - 35
        }

        that.setData({// 设置高度
          tbodyMaxHeight: tbodyMaxHeight
        });
      }
    })
    //初始化表格
    let new_colums = [
      { title: '断电扣款', field: 'outage_deduction', width: '200' },
      // { title: '大区断电扣款', field: 'region_deduction', width: '200' },
      // { title: '大区总断电扣款', field: 'regional_director_deduction', class: '', width: '200' }, // class可以修改单元样式
      { title: '断电机柜数', field: 'outage_count', class: '', width: '200' }
    ]
    that.setData({columns:new_colums})

    app.authLogin(function() {
      let role_slug = app.userData.data.role_slug
    
      that.setData({
        role_slug: role_slug
      })
      let columns = that.data.columns
      if(role_slug == 'regional_director' || role_slug == 'region'){
        columns.unshift({ title: '归属人', field: 'bd_name', width: '120' })
        columns.unshift({ title: '大区', field: 'region_name', width: '120' })
      }else if(role_slug == 'general_manager' || role_slug == 'chairman' || role_slug == 'logistical_commissioner' || role_slug == 'chairman_assistant'){
        columns.unshift({ title: '归属人', field: 'bd_name', width: '120' })
        columns.unshift({ title: '大区', field: 'region_name', width: '120' })
        columns.unshift({ title: '大区总', field: 'regional_director_name', width: '130' })
      }
      columns.unshift({ title: '操作', field: 'operate', width: '150' })

      that.setData({
        columns: columns
      })

      if(role_slug === 'chairman' || role_slug === 'chairman_assistant' || role_slug === 'generat_manager' || role_slug === 'logistical_commissioner'){//董事长
        that.initPicker('regional_director','')
        that.initPicker('region','')
        that.initPicker('bd','')
      }else if(role_slug === 'regional_director'){//大区总
        that.initPicker('region','')
        that.initPicker('bd','')
      }else if(role_slug === 'region'){//大区
        // that.initPicker('bd','')
      }

      that.getList()
    })
  },
  // 下拉框事件
  handleOk(value, column, e) {
    let type = e.target.dataset.type
    if(type === '1'){//团队
      this.setData({
        regional_director: value,
        region: '',
        person: ''
      })

      this.initPicker('region',value)
      this.initPicker('bd','')
    }else if(type === '2'){//组别
      this.setData({
        region: value,
        person: ''
      })

      this.initPicker('bd',value)
    }else if(type === '3'){//归属人
      this.setData({
        person: value
      })
    }

    this.getList()
  },
  // 初始化团队、组别、归属人的下拉值
  async initPicker(type,id){
    let that = this,role_slug = this.data.role_slug,obj = {};

    obj = {
      type: type,
      id: id
    }

    let res = await myRequest('/api/get-select-user','GET',obj)

    if(res.errcode === 0){
      let itemList = [{ label: '全部', value: ''}];

      let list = res.data
      for(let i = 0;i< list.length;i++){
        itemList.push({ label: list[i].label, value: list[i].value})
      }

      if(type === 'regional_director'){
        that.setData({
          regional_director_list:itemList
        })
      }else if(type === 'region'){
        that.setData({
          region_list:itemList
        })
      }else if(type === 'bd'){
        that.setData({
          person_list:itemList
        })
      }
    }
  },
  //获取列表
  async getList() {
    let that = this
    let data = {}

    let role_slug = app.userData.data.role_slug
    if(role_slug === 'chairman' || role_slug === 'chairman_assistant' || role_slug === 'generat_manager' || role_slug === 'logistical_commissioner'){//董事长
      data = {
        regional_director: this.data.regional_director,
        region: this.data.region,
        person: this.data.person
      }
    }else if(role_slug === 'regional_director'){//大区总
      data = {
        region: this.data.region,
        person: this.data.person
      }
    }else if(role_slug === 'region'){//大区
      data = {
        person: this.data.person
      }
    }else if(role_slug === 'bd'){//大区

    }

    let res = await myRequest('/outage-api/statistics','GET',data)
    that.setData({
      tableData:res.data || []
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.data.errmsg,
        duration: 2000
      });
    }
  },
  handleTapApproval(e) {
    // 带参数的跳转，从 page/index 的 onLoad 函数的 query 中读取 xx
    dd.navigateTo({ url: '/pages/approveList/approveList'})
  },
  // 跳转页面
  dumpTab(e) {
    let type = e.target.dataset.type,url = '';
    if(type === '1'){//详情
      url = '../detailTable/detailTable?id=' + e.target.dataset.id
    }

    if(!url) return;
    dd.navigateTo({
      url: url
    });
  }
});
