const app = getApp()
import { myRequest } from '../../utils/http';
Page({
  data: {
    list: []
  },
  onShow() {
    let that = this
    app.authLogin(function() {
      that.getList()
    })
  },
  //获取列表
  async getList() {
    let that = this
    let res = await myRequest('/offset-api/approval-list')
    that.setData({
      list: res.data || []
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
    }
  },
  handleTap(e) {
    let id = e.target.dataset.row.id
    dd.redirectTo({url: '/pages/appointmentApproval/appointmentApproval?id='+id});
  }
});
