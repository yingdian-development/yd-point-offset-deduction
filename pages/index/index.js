const app = getApp()
import openLink from 'dingtalk-jsapi/api/biz/util/openLink';
import { myRequest } from '../../utils/http';
Page({
  data: {
    tableData: [],
    columns: [
      // { title: '操作', field: 'operate', width: '150' },
      { title: '当前审批人', field: 'now_approval_user', width: '150' },
      { title: '审批状态', field: 'status', width: '150' },
      { title: '门店名称', field: 'name', width: '200' },
      { title: '门店ID', field: 'store_id_value', width: '200' }, // class可以修改单元样式
      { title: '提交人', field: 'bd_name', width: '150' },
      { title: '提交时间', field: 'created_at', width: '150' },
      { title: '报备原因', field: 'reason', width: '150' },
      { title: '预计处理完成日期', field: 'report_date', width: '150' }
    ],
    headersLen: 1700,
    tbodyMaxHeight: null,
    role_slug:'',
    userId: '',
    store_poi: '',
    sn_json: '',
    name: ''
  },
  onShow() {
    let that = this
    dd.getSystemInfo({
      success: function (res) {
        let clientHeight = res.windowHeight;// 获取可使用窗口宽度
        let clientWidth = res.windowWidth;// 获取可使用窗口高度
        let ratio = 750 / clientWidth;// 算出比例
        let height = clientHeight * ratio;// 算出高度(单位rpx)
        
        that.setData({
          tbodyMaxHeight: height - 360// 设置高度
        });
      }
    })

    if(!dd.getStorageSync({key: 'token'}).data){
      app.login()
    }

    app.authLogin(function() {

      // if(app.userData.data.role_slug == 'bd' || app.userData.data.role_slug == 'region'){
      //   let myArray =that.data.columns
      //   let fieldToCheck = 'operate'
      //   let isFieldFound = false
      //   for (let i = 0; i < myArray.length; i++) {
      //     if (myArray[i].field === fieldToCheck) {
      //         isFieldFound = true;
      //         break;
      //     }
      //   }
      //   if (!isFieldFound) {
      //     myArray.unshift({ title: '操作', field: fieldToCheck, width: '150' });
     
      //   }

      //   that.setData({
      //     columns: that.data.columns
      //   })
      // }

      that.setData({
        role_slug: app.userData.data.role_slug,
        userId: app.userData.data.id
      })
      that.getList()
    })
  },
  iptChange(value,e) {
    let type = e.target.dataset.type,obj = {}
    if(type == '1'){
      obj = {
        store_poi: value
      }
    }else if(type == '2'){
      obj = {
        sn_json: value
      }
    }else if(type == '3'){
      obj = {
        name: value
      }
    }

    this.setData(obj);
  },
  //获取列表
  async getList() {
    let that = this,obj = {
      // store_poi: this.data.store_poi,
      // sn_json: this.data.sn_json,
      // name: this.data.name
    }

    let res = await myRequest('/offset-api/list','GET',obj)

    that.setData({
      tableData:res.data || []
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.data.errmsg,
        duration: 2000
      });
    }
  },
  handleWithdraw(e){
    let that = this
    my.confirm({
      title: '温馨提示',
      content: '是否确认撤销',
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      success: (result) => {
        if (result.confirm) {
          that.withdraw(e)
        }
      },
    });
  },
  async withdraw(e) {
    let that = this 
    let param = {id:e.target.dataset.id}
    let res = await myRequest('/outage-api/withdrawal','POST',param)
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }

    dd.showToast({
      type: 'none',
      content: '提交成功',
      duration: 2000
    });
    that.getList()
  },
  // 跳转页面
  dumpTab(e) {
    let type = e.target.dataset.type,url = '';
    if(type === '2'){//新增退出
      url = '../form/form?type=1'
    }else if(type === '3'){//待审批
      url = '../approveList/approveList'
    }

    if(!url) return;
    dd.navigateTo({
      url: url
    });
  }
});