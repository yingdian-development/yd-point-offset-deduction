import { Form } from 'antd-mini/es/Form/form';
import { myRequest } from '../../utils/http';
const app = getApp()

Page({
  form: new Form(),
  data: {
    id: '',
    status: '',//0未审批 1已审批
    itemArr:[],
    header: '',
    typeObj:{
      '1': '门店更改摊销审批',
    },
    statusObj: {
      0: '未审批',
      1: '已审批',
      2: '审批已拒绝'
    },
    detail: [],
    flow_log: [],
    permission: '',
    popupVisile: false,
    fileList0: [],
    images0: [],
    width: '',
    height: ''
  },
  onLoad(query) {
    let that = this;
    that.setData({
      id: query.id,
      fileList0: [],
      images0: []
    })
    app.authLogin(function () {
      that.getDetail()
    })
  },
  uploadChange(fileList) {
    // 这里的数据包括上传失败和成功的图片列表，如果需要筛选出上传成功的图片需要在此处理
    console.log('图片列表：', fileList);
  },
  // 上传图片
  uploadImage(e) {
    let that = this,type = e.target.dataset.type;

    if(type === '0'){
      let fileList = that.data['fileList' + type]
      if(fileList.length > 0){
        my.showToast({ content: '只能上传一张图片', duration: 1000 });
        return
      }
    }

    dd.chooseImage({ 
      sourceType: ['camera', 'album'],
      count: 1,
      success: (res1) => {
        console.log('res1', res1);
        dd.showLoading({
          content: '转码中...'
        });
        dd.compressImage({
          filePaths: res1.filePaths,
          compressLevel: 2,
          success: (res2) => {
            console.log(res2)
            // 获取图片信息
            dd.getImageInfo({
              src: res2.filePaths[0],
              success: (res) => {
                console.log(res.width, 'res')
                const { width, height, path } = res;
                that.setData({
                  width,
                  height
                });
                const ctx = dd.createCanvasContext('canvas' + type);
                ctx.drawImage(path, 0, 0, width, height);
                ctx.draw(true, () => {
                  // 获取画布指定区域的 data URL 数据
                  ctx.toDataURL({
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                    destWidth: width,
                    destHeight: height,
                  }).then(dataURL => {
                    console.log(dataURL)  // 获取base64数据
                    return dataURL
                  }).then(res => {
                    dd.hideLoading();
                    that.onUpload(res,type,width,height); // 将base64数据传给后台的方法
                  })
                })
              }
            })
          }
        });
      },
      fail: (e) => {
        console.log('fail----------------------')
      }
    })
  },
  async onUpload(file,type,width,height) {
    let that = this

    let param = {
      images: file
    }

    let res = await myRequest('/offset-api/upload','POST',param)
    if(res.errcode === 0){
      let fileList = that.data['fileList' + type],images= that.data['images' + type]
      fileList.push({
        url: file,
        width: width,
        height: height
      })
      images.push(res.data)

      let obj = {}
      obj['fileList' + type] = fileList
      obj['images' + type] = images
      
      that.setData(obj)
    }else{
      my.showToast({ content: '上传图片失败', duration: 1000 });
    }
  },
  // 点击图片放大
  clickImg(e) {
    dd.previewImage({
      urls: [
        e.target.dataset.src
      ],
      current: 1,
      success: () => {},
      fail: () => {},
      complete: () => {},
    });
  },
  clickPop(){
    this.setData({
      popupVisile: true
    })
  },
  handleRef(ref) {
    this.form.addItem(ref);
  },
  // 关闭弹窗
  handleClose() {
    this.setData({
      popupVisile: false
    })
  },
  async getDetail() {
    let that = this
    my.showLoading({
      content: '数据加载中...',
    });

    let res = await myRequest('/offset-api/approval-detail', 'GET', { id: that.data.id })
    if (res.errcode != 0) {
      my.hideLoading();
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }
    let itemArr = [],data = res.data;

    itemArr = [
      {name:'提交人',value:data.approval_originator_name},
      {name:'提交时间',value:data.created_at},
      {name:'门店名称',value:data.offset.name},
      {name:'门店ID',value:data.offset.store_id_value},
      {name:'预计处理完成日期',value:data.offset.report_date},
      {name:'报备原因',value:data.offset.reason}
    ]

    that.setData({
      detail: res.data || [],
      flow_log: res.data.flow_log || [],
      permission: res.data.permission || '',
      itemArr,
      loading: false
    })
    my.hideLoading();
  },
  async submit(e) {

    let that = this,param = {},type = e.target.dataset.type;
    const values = await this.form.submit();
  
    // if(type === 'no_pass'){
    //   let reason = values.reason
    //   if(reason === '' || reason == null || reason == undefined){
    //     my.showToast({ content: '请输入拒绝理由', duration: 1000 });
    //     return
    //   }

    //   param.reason = reason
    // }

    param.id = that.data.detail.id
    param.type = type
    // let images0 = this.data.images0
    // if(images0.length === 0){
    //   my.showToast({ content: '请上传照片', duration: 1000 });
    //   return
    // }

    // param.comment = values.comment
    // param.comment_photo = images0 ? images0.join(',') : ''

    myRequest('/offset-api/approval', 'POST', param).then(res => {
      if (res.errcode != 0) {
        dd.showToast({
          type: 'none',
          content: res.errmsg,
          duration: 2000
        });
        return
      }
      dd.showToast({
        type: 'none',
        content: '操作成功',
        duration: 2000
      });
      dd.reLaunch({
        url: '/pages/index/index'
      })
    })
  }
});