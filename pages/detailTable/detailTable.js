const app = getApp()
import openLink from 'dingtalk-jsapi/api/biz/util/openLink';
import { myRequest } from '../../utils/http';
Page({
  data: {
    type: '',//首页-详情
    tableData: [],
    columns: [
      { title: '日期', field: 'date', width: '200' },
      { title: '门店名称', field: 'store_name', width: '150' },
      { title: '门店ID', field: 'store_poi', width: '150' },
      { title: '断电扣款', field: 'outage_deduction', class: '', width: '150' },
      { title: '机柜SN', field: 'sn', class: '', width: '150' },
      { title: '归属人', field: 'bd_name', class: '', width: '150' },
      { title: 'BDM', field: 'region_name', width: '200' },
      { title: 'CM', field: 'regional_director_name', class: '', width: '150' }
    ],
    headersLen: 1300,
    tbodyMaxHeight: null,
    role_slug:'',
    userId:'',
    person_id: ''
  },
  onLoad(e) {
    this.setData({
      person_id: e.id
    })
  },
  onShow() {
    let that = this
    dd.getSystemInfo({
      success: function (res) {
        let clientHeight = res.windowHeight;// 获取可使用窗口宽度
        let clientWidth = res.windowWidth;// 获取可使用窗口高度
        let ratio = 750 / clientWidth;// 算出比例
        let height = clientHeight * ratio;// 算出高度(单位rpx)
        
        that.setData({
          tbodyMaxHeight: height// 设置高度
        });
      }
    })
    app.authLogin(function() {

      // if(app.userData.data.role_slug == 'bd' || app.userData.data.role_slug == 'region'){
      //   let myArray =that.data.columns
      //   let fieldToCheck = 'operate'
      //   let isFieldFound = false
      //   for (let i = 0; i < myArray.length; i++) {
      //     if (myArray[i].field === fieldToCheck) {
      //         isFieldFound = true;
      //         break;
      //     }
      //   }
      //   if (!isFieldFound) {
      //     myArray.unshift({ title: '操作', field: fieldToCheck, width: '150' });
     
      //   }

      //   that.setData({
      //     columns: that.data.columns
      //   })
      // }

      that.setData({
        role_slug: app.userData.data.role_slug,
        userId: app.userData.data.id
      })
      that.getList()
    })
  },
  //获取列表
  async getList() {
    let that = this

    let res = await myRequest('/outage-api/outage-report-list','GET',{person_id:that.data.person_id})

    that.setData({
      tableData:res.data || []
    })
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.data.errmsg,
        duration: 2000
      });
    }
  },
  handleTapAdd(e) {
    dd.navigateTo({ url: '/pages/form/form'})
  },
  handleWithdraw(e){
    let that = this
    my.confirm({
      title: '温馨提示',
      content: '是否确认撤销',
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      success: (result) => {
        if (result.confirm) {
          that.withdraw(e)
        }
      },
    });
  },
  async withdraw(e) {
    let that = this
    let param = {id:e.target.dataset.id}
    let res = await myRequest('/outage-api/withdrawal','POST',param)
    if (res.errcode != 0) {
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }
    dd.showToast({
      type: 'none',
      content: '提交成功',
      duration: 2000
    });
    that.getList()
  },
  // 跳转页面
  dumpTab(e) {
    let type = e.target.dataset.type,url = '';
    if(type === '1'){//详情
      url = '../detail/detail?id=' + e.target.dataset.id
    }

    if(!url) return;
    dd.navigateTo({
      url: url
    });
  }
});