import { myRequest } from '../../utils/http';
const app = getApp()

Page({
  data: {
    id: '',
    detail: [],
    popupVisile: false
  },
  onLoad(query) {
    let that = this;
    that.setData({
      id: query.id
    })
    app.authLogin(function () {
      that.getDetail()
    })
  },
  // 点击图片放大
  clickImg(e) {
    dd.previewImage({
      urls: [
        e.target.dataset.src
      ],
      current: 1,
      success: () => {},
      fail: () => {},
      complete: () => {},
    });
  },
  // 关闭弹窗
  handleClose() {
    this.setData({
      popupVisile: false
    })
  },
  async getDetail() {
    let that = this
    my.showLoading({
      content: '数据加载中...',
    });

    let res = await myRequest('/withdrawal-api/store-detail', 'GET', { id: that.data.id })
    if (res.errcode != 0) {
      my.hideLoading();
      dd.showToast({
        type: 'none',
        content: res.errmsg,
        duration: 2000
      });
      return
    }
    that.setData({
      detail: res.data || [],
      loading: false
    })
    my.hideLoading();
  },
  async submit(e) {

    let that = this
    let param = {}
    param.id = that.data.detail.id
    param.type = e.target.dataset.type

    myRequest('/outage-api/approval', 'POST', param).then(res => {
      if (res.errcode != 0) {
        dd.showToast({
          type: 'none',
          content: res.errmsg,
          duration: 2000
        });
        return
      }
      dd.showToast({
        type: 'none',
        content: '操作成功',
        duration: 2000
      });
      dd.reLaunch({
        url: '/pages/home/home'
      })
    })
  }
});